package com.chhaya.retrofit6_8.callback;

public interface OnBaseCallback<T> {

    void onSuccess(T response);
    void onError(String message);

}
