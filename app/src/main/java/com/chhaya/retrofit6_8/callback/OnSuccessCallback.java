package com.chhaya.retrofit6_8.callback;

public interface OnSuccessCallback {
    void onSuccess();
}
