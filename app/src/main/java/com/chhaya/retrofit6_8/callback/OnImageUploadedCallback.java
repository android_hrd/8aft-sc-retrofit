package com.chhaya.retrofit6_8.callback;

import com.chhaya.retrofit6_8.data.api.response.UploadSingleResponse;

public interface OnImageUploadedCallback {

    void onUploadSuccess(UploadSingleResponse response);
    void onUploadFail(String message);

}
