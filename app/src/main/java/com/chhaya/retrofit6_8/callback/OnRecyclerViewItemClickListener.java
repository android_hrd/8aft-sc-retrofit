package com.chhaya.retrofit6_8.callback;

public interface OnRecyclerViewItemClickListener {

    void onItemClicked(int position);

}
