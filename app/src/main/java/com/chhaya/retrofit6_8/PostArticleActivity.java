package com.chhaya.retrofit6_8;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chhaya.retrofit6_8.callback.OnImageUploadedCallback;
import com.chhaya.retrofit6_8.callback.OnSuccessCallback;
import com.chhaya.retrofit6_8.data.api.response.UploadSingleResponse;
import com.chhaya.retrofit6_8.data.api.service.repository.ArticleRepository;
import com.chhaya.retrofit6_8.data.api.service.repository.UploadImageRepository;
import com.chhaya.retrofit6_8.data.entity.ArticleEntity;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.kroegerama.imgpicker.ButtonType;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PostArticleActivity extends AppCompatActivity
        implements View.OnClickListener, BottomSheetImagePicker.OnImagesSelectedListener {

    private final static int REQUEST_CODE = 200;

    private ImageView imageArticleInput;
    private EditText editTitleInput;
    private EditText editDescInput;
    private Button btnPost;
    private FrameLayout imageContainer;

    private Uri imageUri;

    private UploadImageRepository uploadImageRepository;
    private ArticleRepository atArticleRepository;

    private void initViews() {
        imageArticleInput = findViewById(R.id.image_article_input);
        editTitleInput = findViewById(R.id.edit_title_input);
        editDescInput = findViewById(R.id.edit_desc_input);
        btnPost = findViewById(R.id.button_post);
        imageContainer = findViewById(R.id.image_container);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_article);

        // init views
        initViews();

        // register event onclick on views
        imageArticleInput.setOnClickListener(this);
        btnPost.setOnClickListener(this);

        uploadImageRepository = new UploadImageRepository();
        atArticleRepository = new ArticleRepository();
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.image_article_input: {

                // Choose image from gallery:
                /*Intent chooseImageIntent = new Intent(Intent.ACTION_PICK);
                chooseImageIntent.setType("image/*");
                startActivityForResult(chooseImageIntent, REQUEST_CODE);*/
                new BottomSheetImagePicker.Builder("SC")
                        .cameraButton(ButtonType.Button)
                        .galleryButton(ButtonType.Button)
                        .singleSelectTitle(R.string.demo_text_title)
                        .peekHeight(R.dimen.test)
                        .columnSize(R.dimen.test)
                        .requestTag("single")
                        .show(getSupportFragmentManager(), null);

                break;
            }
            case R.id.button_post: {

                // Upload image here:
                uploadImageRepository.uploadSingleImage(
                        getImageFormData(imageUri.getPath()),
                        new OnImageUploadedCallback() {
                            @Override
                            public void onUploadSuccess(UploadSingleResponse response) {
                                Log.d("TAG", "Success = " + response);
                                // Post article:
                                atArticleRepository.insertOneArticle(getArticleEntity(response.getData()), new OnSuccessCallback() {
                                    @Override
                                    public void onSuccess() {
                                        Toast.makeText(PostArticleActivity.this, "Article uploaded..!",
                                                Toast.LENGTH_LONG).show();
                                        finish();
                                    }
                                });
                            }

                            @Override
                            public void onUploadFail(String message) {
                                Log.e("TAG", message);
                            }
                        }
                );

            }
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                imageUri = data.getData();
                imageArticleInput.setImageURI(imageUri);
                Log.d("TAG", "Image Uri = " + imageUri);
            }
        }
    }*/

    private MultipartBody.Part getImageFormData(String imagePath) {
        // create image file:
        File file = new File(imagePath);
        Log.d("TAG001", imagePath);

        // create request body:
        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // Create multipartbody.part:
        MultipartBody.Part part = MultipartBody.Part.createFormData(
                "FILE",
                file.getName(),
                fileRequestBody);

        return part;
    }

    private ArticleEntity getArticleEntity(String imagePath) {
        return new ArticleEntity(
                editTitleInput.getText().toString(),
                editDescInput.getText().toString(),
                imagePath
        );
    }

    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        imageContainer.removeAllViews();
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.image_picker, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            imageUri = uri;
        }
    }
}
