package com.chhaya.retrofit6_8.data.api.response;

import com.chhaya.retrofit6_8.data.entity.ArticleEntity;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticlesReponse {

    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private List<ArticleEntity> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ArticleEntity> getData() {
        return data;
    }

    public void setData(List<ArticleEntity> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ArticlesReponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
