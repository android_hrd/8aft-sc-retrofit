package com.chhaya.retrofit6_8.data.api.service;

import com.chhaya.retrofit6_8.data.api.response.ArticlesReponse;
import com.chhaya.retrofit6_8.data.api.response.PostArticleResponse;
import com.chhaya.retrofit6_8.data.entity.ArticleEntity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ArticlesReponse> findAll(@Query("page") long page,
                                  @Query("limit") long limit);

    @POST("v1/api/articles")
    Call<PostArticleResponse> insertOne(@Body ArticleEntity articleEntity);

    @GET("v1/api/articles/{id}")
    Call<PostArticleResponse> findOne(@Path("id") int id);

}
