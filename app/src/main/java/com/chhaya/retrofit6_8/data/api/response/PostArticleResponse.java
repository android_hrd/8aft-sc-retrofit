package com.chhaya.retrofit6_8.data.api.response;

import com.chhaya.retrofit6_8.data.entity.ArticleEntity;
import com.google.gson.annotations.SerializedName;

public class PostArticleResponse {
    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private ArticleEntity data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArticleEntity getData() {
        return data;
    }

    public void setData(ArticleEntity data) {
        this.data = data;
    }
}
