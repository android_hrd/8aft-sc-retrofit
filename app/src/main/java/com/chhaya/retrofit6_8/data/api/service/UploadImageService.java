package com.chhaya.retrofit6_8.data.api.service;

import com.chhaya.retrofit6_8.data.api.response.UploadSingleResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadImageService {

    @Multipart
    @POST("v1/api/uploadfile/single")
    Call<UploadSingleResponse> uploadSingle(@Part MultipartBody.Part file);

}
