package com.chhaya.retrofit6_8.data.api.service.repository;

import android.util.Log;

import com.chhaya.retrofit6_8.callback.OnImageUploadedCallback;
import com.chhaya.retrofit6_8.data.api.config.ServiceGenerator;
import com.chhaya.retrofit6_8.data.api.response.UploadSingleResponse;
import com.chhaya.retrofit6_8.data.api.service.UploadImageService;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadImageRepository {

    private UploadImageService uploadImageService;

    public UploadImageRepository() {
        uploadImageService = ServiceGenerator.createService(UploadImageService.class);
    }

    public void uploadSingleImage(MultipartBody.Part file, final OnImageUploadedCallback callback) {
        Log.e("TAG", "OKAY");
        uploadImageService.uploadSingle(file).enqueue(new Callback<UploadSingleResponse>() {
            @Override
            public void onResponse(Call<UploadSingleResponse> call, Response<UploadSingleResponse> response) {
                Log.e("TAG", "Success");
                Log.e("TAG", response + "");
                if (response.isSuccessful()) {
                    Log.e("TAG", "Success");
                    callback.onUploadSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<UploadSingleResponse> call, Throwable t) {
                Log.e("TAG", t.getMessage());
                callback.onUploadFail(t.getMessage());
            }
        });
    }

}
