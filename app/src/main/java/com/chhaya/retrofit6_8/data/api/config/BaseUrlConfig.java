package com.chhaya.retrofit6_8.data.api.config;

public class BaseUrlConfig {
    public static final String BASE_URL_AMS = "http://110.74.194.124:15011/";
    public static final String BASE_URL_JSON_PLACEHOLDER = "https://jsonplaceholder.typicode.com/";
}
