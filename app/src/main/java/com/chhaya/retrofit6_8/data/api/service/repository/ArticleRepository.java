package com.chhaya.retrofit6_8.data.api.service.repository;

import android.util.Log;

import com.chhaya.retrofit6_8.callback.OnBaseCallback;
import com.chhaya.retrofit6_8.callback.OnSuccessCallback;
import com.chhaya.retrofit6_8.data.api.config.ServiceGenerator;
import com.chhaya.retrofit6_8.data.api.response.ArticlesReponse;
import com.chhaya.retrofit6_8.data.api.response.PostArticleResponse;
import com.chhaya.retrofit6_8.data.api.service.ArticleService;
import com.chhaya.retrofit6_8.data.entity.ArticleEntity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private final static String TAG = "Article";

    private ArticleService articleService;

    public ArticleRepository() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    public void findOneArticle(int id, OnBaseCallback<PostArticleResponse> callback) {
        articleService.findOne(id).enqueue(new Callback<PostArticleResponse>() {
            @Override
            public void onResponse(Call<PostArticleResponse> call, Response<PostArticleResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<PostArticleResponse> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public ArticlesReponse findAllArticles(int page, int limit, final OnSuccessCallback callback) {
        final ArticlesReponse dataSet = new ArticlesReponse();
        articleService.findAll(page, limit).enqueue(new Callback<ArticlesReponse>() {
            @Override
            public void onResponse(Call<ArticlesReponse> call, Response<ArticlesReponse> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "onResponse: Successfully");
                    dataSet.setCode(response.body().getCode());
                    dataSet.setMessage(response.body().getMessage());
                    dataSet.setData(response.body().getData());
                    callback.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<ArticlesReponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
        return dataSet;
    }

    public void insertOneArticle(ArticleEntity articleEntity, final OnSuccessCallback callback) {
        articleService.insertOne(articleEntity).enqueue(new Callback<PostArticleResponse>() {
            @Override
            public void onResponse(Call<PostArticleResponse> call, Response<PostArticleResponse> response) {
                callback.onSuccess();
            }

            @Override
            public void onFailure(Call<PostArticleResponse> call, Throwable t) {
                Log.e("TAG", t.getMessage());
            }
        });
    }

}
