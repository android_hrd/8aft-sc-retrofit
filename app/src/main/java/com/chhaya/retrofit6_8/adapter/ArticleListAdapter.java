package com.chhaya.retrofit6_8.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chhaya.retrofit6_8.R;
import com.chhaya.retrofit6_8.callback.OnRecyclerViewItemClickListener;
import com.chhaya.retrofit6_8.data.api.response.ArticlesReponse;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ArticleViewHolder> {

    private Context context;
    private ArticlesReponse dataSet;
    private OnRecyclerViewItemClickListener listener;

    public ArticleListAdapter(Context context, ArticlesReponse dataSet) {
        this.context = context;
        this.dataSet = dataSet;
        try {
            this.listener = (OnRecyclerViewItemClickListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public ArticleListAdapter.ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.article_item_layout, parent, false);
        return new ArticleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleListAdapter.ArticleViewHolder holder, int position) {
        holder.textTitle.setText(dataSet.getData().get(position).getTitle());
        holder.textDesc.setText(dataSet.getData().get(position).getDesc());
        Glide.with(context).load(dataSet.getData().get(position).getImageUrl()).placeholder(R.drawable.placeholder2).into(holder.imageArticle);
    }

    @Override
    public int getItemCount() {
        return dataSet.getData().size();
    }

    class ArticleViewHolder extends RecyclerView.ViewHolder {
        TextView textTitle, textDesc, textDelete, textUpdate;
        ImageView imageArticle;
        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_article_title);
            textDesc = itemView.findViewById(R.id.text_article_description);
            textDelete = itemView.findViewById(R.id.text_article_delete);
            textUpdate = itemView.findViewById(R.id.text_article_update);
            imageArticle = itemView.findViewById(R.id.image_article);

            itemView.setOnClickListener(v -> listener.onItemClicked(getAdapterPosition()));

        }

    }

}
