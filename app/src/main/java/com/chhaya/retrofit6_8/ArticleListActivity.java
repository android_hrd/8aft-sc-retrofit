package com.chhaya.retrofit6_8;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.chhaya.retrofit6_8.adapter.ArticleListAdapter;
import com.chhaya.retrofit6_8.callback.OnRecyclerViewItemClickListener;
import com.chhaya.retrofit6_8.callback.OnSuccessCallback;
import com.chhaya.retrofit6_8.data.api.response.ArticlesReponse;
import com.chhaya.retrofit6_8.data.api.service.repository.ArticleRepository;

public class ArticleListActivity extends AppCompatActivity
    implements OnRecyclerViewItemClickListener {

    private boolean isView = false;

    private ArticlesReponse dataSet;
    private RecyclerView rvArticle;
    private ArticleListAdapter adapter;
    private ArticleRepository articleRepository;
    private ProgressBar pbLoading;
    private SwipeRefreshLayout pullRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        // init recyclerview
        rvArticle = findViewById(R.id.rv_article);

        pbLoading = findViewById(R.id.pb_loading);

        pullRefresh = findViewById(R.id.pull_refresh);


        pullRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("TAG", "onRefresh");
                fetchData();
            }
        });


        // init article repo
        articleRepository = new ArticleRepository();
        fetchData();

    }

    // Create option menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add: {
                // Open post activity
                Intent postIntent = new Intent(this, PostArticleActivity.class);
                startActivity(postIntent);

                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        adapter = new ArticleListAdapter(this, dataSet);
        rvArticle.setAdapter(adapter);
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        adapter.notifyDataSetChanged();
    }


    private void fetchData() {
        dataSet = articleRepository.findAllArticles(1, 15, new OnSuccessCallback() {
            @Override
            public void onSuccess() {
                setupRecyclerView();
                pbLoading.setVisibility(View.GONE);
                pullRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isView) fetchData();
        isView = false;
    }

    @Override
    public void onItemClicked(int position) {
        Log.d("TAG CLICK", position + " is clicked");
        isView = true;
        // Open article detail activity:
        Intent detailIntent = new Intent(
                this,
                ArticleDetailActivity.class);
        int articleId = dataSet.getData().get(position).getId();
        detailIntent.putExtra("articleId", articleId);
        startActivity(detailIntent);
    }
}
