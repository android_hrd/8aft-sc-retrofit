package com.chhaya.retrofit6_8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chhaya.retrofit6_8.callback.OnBaseCallback;
import com.chhaya.retrofit6_8.data.api.response.PostArticleResponse;
import com.chhaya.retrofit6_8.data.api.service.repository.ArticleRepository;

public class ArticleDetailActivity extends AppCompatActivity {

    private ImageView imageArticleDetail;
    private TextView textTitleDetail;
    private TextView textDescDetail;

    private int articleId;

    private ArticleRepository articleRepository;

    private void initViews() {
        imageArticleDetail = findViewById(R.id.image_article_detail);
        textTitleDetail = findViewById(R.id.article_title_detail);
        textDescDetail = findViewById(R.id.article_desc_detail);
    }

    private void bindDataToViews() {
        articleRepository.findOneArticle(articleId, new OnBaseCallback<PostArticleResponse>() {
            @Override
            public void onSuccess(PostArticleResponse response) {
                Glide.with(ArticleDetailActivity.this).load(response.getData().getImageUrl())
                        .placeholder(R.drawable.placeholder2)
                        .into(imageArticleDetail);
                textTitleDetail.setText(response.getData().getTitle());
                textDescDetail.setText(response.getData().getDesc());
            }

            @Override
            public void onError(String message) {
                Toast.makeText(ArticleDetailActivity.this,
                        message,
                        Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        // init view here:
        initViews();

        // init article repo:
        articleRepository = new ArticleRepository();

        // call new intent coming:
        onNewIntent(getIntent());

        // bind data to views:
        bindDataToViews();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Bundle extras = intent.getExtras();

        if (extras != null) {
            articleId = extras.getInt("articleId");
        }
    }
}
