package com.chhaya.retrofit6_8;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.os.Bundle;
import android.util.Log;

import com.chhaya.retrofit6_8.data.api.config.ServiceGenerator;
import com.chhaya.retrofit6_8.data.api.response.ArticlesReponse;
import com.chhaya.retrofit6_8.data.api.service.ArticleService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArticleService articleService =
                ServiceGenerator.createService(ArticleService.class);

        Call<ArticlesReponse> callArticle = articleService.findAll(1, 5);

        // work in background
        callArticle.enqueue(new Callback<ArticlesReponse>() {
            @Override
            public void onResponse(Call<ArticlesReponse> call, Response<ArticlesReponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ArticlesReponse articlesReponse =
                            response.body();
                    Log.d(TAG, "onResponse: " + articlesReponse.getData().get(0));
                }
            }

            @Override
            public void onFailure(Call<ArticlesReponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

}
